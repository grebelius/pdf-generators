#!/bin/sh

# ---------------------------------------------------------------------------
# Functions that create the ram disk and copies the Indesign template onto it
# ---------------------------------------------------------------------------

function createRamDiskAndCopyFiles {
    logThis "1" "Start of function"

    VOLUME_NAME=RamDisk-$(date '+%Y-%m-%d_%H.%M.%S')
    RAM_DISK_VOLUME_NAME="$VOLUME_NAME"
    SIZE_IN_MB=200
    DEVICE=`hdiutil attach -nobrowse -nomount ram://$(($SIZE_IN_MB*2048))`

    diskutil erasevolume HFS+ $RAM_DISK_VOLUME_NAME $DEVICE

    logThis "1" "Copying template to $RAM_DISK_VOLUME_NAME"

    export RAM_DISK_PATH="/Volumes/$RAM_DISK_VOLUME_NAME"
    export TEMPLATE_FILE_PATH="$RAM_DISK_PATH/template.indt"
    cp ./resources/template.indt "$TEMPLATE_FILE_PATH"
    if [ ! -f "$TEMPLATE_FILE_PATH" ] ; then
        logThis "2" "Copy of template.indt failed" 1
        logThis "2" "Exiting script…" 1
        exit 1
    fi
    logThis "1" "End of function"
}

function ramDiskForDevMode {
    logThis "1" "Start of function"
    logThis "1" "Assuming RamDisk is already mounted and template file copied."
    VOLUME_NAME="$1"
    RAM_DISK_VOLUME_NAME="$VOLUME_NAME"
    RAM_DISK_PATH="/Volumes/$RAM_DISK_VOLUME_NAME"
    logThis "1" "RAM_DISK_VOLUME_NAME is set to $RAM_DISK_VOLUME_NAME"
    logThis "1" "RAM_DISK_PATH is set to $RAM_DISK_PATH"
    logThis "1" "End of function"
}