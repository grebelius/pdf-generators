#!/usr/bin/env bash

if [[ ! $# -eq 3 ]] ; then
echo "$0 directory_path start_volume end_volume"
exit 0
fi

TEMPDIR="${1%/}"
DIR="$(realpath "$TEMPDIR")"
START="$2"
END="$3"

if [ -f "$DIR/_numberstopipetoparallel.txt" ]; then
	mv "$DIR/_numberstopipetoparallel.txt" "$DIR/.numberstopipetoparallel$(date '+%Y-%m-%d_%H.%M.%S').txt.bak"
fi

seq "$START" "$END" > "$DIR/_numberstopipetoparallel.txt"

relabelPagesInFile ()
{
  DIR="$1"
  VOLUMENUMBER="$2"
  FILEPATH="$DIR/One-Trillion-Digits-of-Pi-$VOLUMENUMBER.pdf"
  FILE="$(basename "$FILEPATH")"

  STARTWITHPAGENUMBER=$((VOLUMENUMBER*386-385))

  if [ -f "$FILEPATH" ]; then
      echo "  => $FILE - Konverterar..."
      BASENAME="$(basename "$FILE")"

      tempfoo="$(basename "$0")"
      tempfoo+="$BASENAME"
      TMPFILE0="$(mktemp /Volumes/RamDisk/"$tempfoo".XXXXXX)" || exit 1
      TMPFILE1="$(mktemp /Volumes/RamDisk/"$tempfoo".XXXXXXX)" || exit 1
      TMPFILE2="$(mktemp /Volumes/RamDisk/"$tempfoo".XXXXXXXX)" || exit 1
      TMPFILE3="$(mktemp /Volumes/RamDisk/"$tempfoo".XXXXXXXXX)" || exit 1
      TMPFILE4="$(mktemp /Volumes/RamDisk/"$tempfoo".XXXXXXXXXX)" || exit 1

      cp "$FILEPATH" "$TMPFILE0"
      qpdf --qdf "$TMPFILE0" "$TMPFILE1"
      sed "s/\/St 1111/\/St $STARTWITHPAGENUMBER/" < "$TMPFILE1" > "$TMPFILE2"
      fix-qdf < "$TMPFILE2" > "$TMPFILE3"
      qpdf --linearize --stream-data=compress "$TMPFILE3" "$TMPFILE4"

      cp "$TMPFILE4" "$FILEPATH"
			xattr -w isRelabeled "yes" "$FILEPATH"
      rm -f "$TMPFILE0" "$TMPFILE1" "$TMPFILE2" "$TMPFILE3" "$TMPFILE4"
  fi
}

testIfFileShouldBeRelabeled ()
{
  # Check if file exists. Return 1 if not.
	if [ ! -f "$1/One-Trillion-Digits-of-Pi-$2.pdf" ]; then
		echo "Filen saknas: One-Trillion-Digits-of-Pi-$2.pdf"
		return 1
	fi

  # Check if file is already relabeled. Return 1 if that's the case.
  if xattr -l "$1/One-Trillion-Digits-of-Pi-$2".pdf | grep -q 'isRelabeled'; then
		return 1
	fi

  # Check if volume number is to low to matter. If so, set isRelabeled to notNeeded.
	if (( "$2" < 2592 )); then
    xattr -w isRelabeled "notNeeded" "$1/One-Trillion-Digits-of-Pi-$2.pdf"
		return 1
	fi

  # Look for the dotfile from an earlier version of the script and remove it if it exists. And set isRelabeled to yes.
	if [ -f "$1/.One-Trillion-Digits-of-Pi-$2.pdf" ]; then
    xattr -w isRelabeled "yes" "$1/One-Trillion-Digits-of-Pi-$2.pdf"
    rm "$1/.One-Trillion-Digits-of-Pi-$2.pdf"
    return 1
	fi

  # If none of the above tests evaluate to true, go ahead and run the relabel function.
	relabelPagesInFile "$1" "$2"
}

# Export functions so that they are available to the parallel run subshells
export -f relabelPagesInFile
export -f testIfFileShouldBeRelabeled

# Run the jobs in parallel
parallel -j0 --bar  testIfFileShouldBeRelabeled "$DIR" {} :::: "$DIR/_numberstopipetoparallel.txt"
