#!/usr/bin/env bash

########################
### Constants        ###
########################
konstnas="/Volumes/DNA"
script_log="$konstnas/logs/Scriptlog-$(date '+%Y-%m-%d_%H.%M.%S').log"

# RAW_PI_FILES_FOLDER_PATH="/Volumes/lacie-raid-1/million"
# PAGENUMBER_FILES_FOLDER_PATH="/Volumes/lacie-raid-1/pagenumbers"
# RENDER_FOLDER_PATH="/Volumes/lacie-raid-1/renders/renders"

### End of constants ###
########################


function checkIfNasVolumeIsMounted {
    if ! df | awk '{print $NF}' | grep -Ex "$konstnas"
    then
        echo "😱 NAS volume DNA is not mounted. Exiting script…"
        exit 1
    fi
}
checkIfNasVolumeIsMounted
echo "NAS volume DNA is mounted."

# ----------------------------------------------------------------------

#################
### Functions ###
#################

### Import logThis function
source ./lib/logthis.sh

### Import startup functions for launching Indesign and checking dependencies and indices
source ./lib/startupfunctions.sh

### Import Ram Disk functions
source ./lib/ramdiskfunctions.sh

### Import render pdf functions
source ./lib/renderfunctions.sh

# The startTraversingTheIndexTree function assumes the root index "_render_folders.index" is in place
# and starts visiting the render folders one by one. In each folder it checks for the "_missing_files.index"
# and, if it finds it, sends every missing volume number to the renderFunction.
#
# The startTraversingTheIndexTree function should receive no argument
# function startTraversingTheIndexTree {
#     while IFS= read -r sub_folder || [ -n "$sub_folder" ]; do
#         logThis "0" "Now inside subfolder $sub_folder"

#         if [ -f "$sub_folder/_missing_files.index" ] ; then
#             while IFS= read -r missing_files || [ -n "$missing_files" ]; do
#                 logThis "0" "Sending missing file $missing_files to renderFunction"
#                 renderFunction $sub_folder $missing_files
#             done < "$sub_folder/_missing_files.index"
#         else
#             logThis "0" "Found no _missing_files.index in $sub_folder. Moving on…"
#         fi

#     done < "$RENDER_FOLDER_PATH/_render_folders.index"
# }

function startTraversingTheFolderStructure {
    local chromosome_array=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 x y mt)

    for chromosome_number in "${chromosome_array[@]}"; do
        local chromosome_folder_path="$konstnas/Chromosome_$chromosome_number/split_files"
        for text_file_number in `seq 1 $(find $chromosome_folder_path -type f -name "*.txt" | wc -l)`; do
            # echo "$ram_disk_path"
            renderFunction  "$chromosome_number" "$text_file_number" "$ram_disk_path"
            exit 1
        done
        exit 1
    done

    # while IFS= read -r sub_folder || [ -n "$sub_folder" ]; do
    #     logThis "0" "Now inside subfolder $sub_folder"

    #     if [ -f "$sub_folder/_missing_files.index" ] ; then
    #         while IFS= read -r missing_files || [ -n "$missing_files" ]; do
    #             logThis "0" "Sending missing file $missing_files to renderFunction"
    #             renderFunction $sub_folder $missing_files
    #         done < "$sub_folder/_missing_files.index"
    #     else
    #         logThis "0" "Found no _missing_files.index in $sub_folder. Moving on…"
    #     fi

    # done < "$RENDER_FOLDER_PATH/_render_folders.index"
}


### End of functions ###
########################

# ----------------------------------------------------------------------

######################
### Main execution ###
######################

logThis "1" "Script started by [$user_name]" 1

## Launch Indesign and see if everything is in place
# launchIndesign
# checkIfDependenciesAreInstalled
# checkIfIndicesExists
# checkIfFontsAreInstalled
# createRamDiskAndCopyFiles
ramDiskForDevMode "DNA-RamDisk-2019-08-27_10.25.05"

## Start generating files according to the indices

startTraversingTheFolderStructure

logThis "1" "Script reached the end. Exiting."
### End of main execution ###
#############################