#!/usr/bin/env bash

VOLUME_NAME=RamDisk
SIZE_IN_MB=500
DEVICE=`hdiutil attach -nobrowse -nomount ram://$(($SIZE_IN_MB*2048))`

diskutil erasevolume HFS+ $VOLUME_NAME $DEVICE