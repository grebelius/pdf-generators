#!/usr/bin/env bash

# Check if correct number of arguments are passed in
if [[ ! $# -eq 1 ]] ; then
    echo ""
    echo "😱 Please pass in the root render folder as argument 1"
    echo "and the destination folder as argument 2."
    echo "  Example:"
    echo "  $0 /Volumes/lacie-raid-1/renders/renders /Volumes/Pi/renders-finished"
    echo "Exiting script…"
    exit 0
fi

# Make sure the passed in folder doesn't have a trailing slash
temp_root_render_folder="${1%/}"
temp_destination_folder="${2%/}"
root_render_folder="$(realpath "$temp_root_render_folder")"
destination_folder="$(realpath "$temp_destination_folder")"

########################
### Constants        ###
########################
konstnas="/Volumes/Pi"


#################
### Functions ###
#################

function checkIfNasVolumeIsMounted {
    if ! df | awk '{print $NF}' | grep -Ex "$konstnas"
    then
        echo "😱 NAS volume Pi is not mounted. Exiting script…"
        exit 1
    fi
}

checkIfNasVolumeIsMounted
echo "NAS volume Pi is mounted."

