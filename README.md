# Fork of [pdfmeta](https://gitlab.com/grebelius/pdfmeta)

Scripts to generate and edit art books. Every project has its own directory.
The scripts are self explanatory and provides useful feedback when run.