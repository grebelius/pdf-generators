#!/bin/sh

# ----------------------------------------------------------------------------------
# Functions that render the pdfs with a little help from Indesign, exiftool and qpdf
# ----------------------------------------------------------------------------------

# The function renderFunction receives 2 arguments.
# $1 is the desired folder path for the pdf.
# $2 is the volume number.
function renderFunction {
    local render_folder_path="$1"
    local volume_number="$2"

    # First, check if file exists. If so, remove from index
    if [ -f "$render_folder_path/One-Trillion-Digits-of-Pi-$volume_number.pdf" ] ; then
        logThis "1" "$volume_number already generated" 1
        logThis "1" "Removing $volume_number from index" 1
        sed -i '' "/$volume_number/d" "$render_folder_path/_missing_files.index"
        return 1
    fi

    createTempFolderInRamDisk $volume_number
    copyFilesToRamDisk $volume_number
    renderPdfWithOsascript $RAM_DISK_VOLUME_NAME $volume_number
    cleanUpPdfMetadata $RAM_DISK_VOLUME_NAME $volume_number
    relabelPdf $RAM_DISK_VOLUME_NAME $volume_number
    addNewPdfMetadata $RAM_DISK_VOLUME_NAME $volume_number
    linearizeAndCompressPdf $RAM_DISK_VOLUME_NAME $volume_number
    savePdfToRenderFolder $RAM_DISK_VOLUME_NAME $volume_number $render_folder_path
    # copyPdfToWebServer $RAM_DISK_VOLUME_NAME $volume_number
    removeTempFolderInRamDisk $volume_number
}

# The function createTempFolderInRamDisk receives 1 argument.
# $1 is the volume number.
# This function should create a temp folder on the RamDisk.
function createTempFolderInRamDisk {
    local volume_number="$1"

    TEMP_FOLDER_PATH="$RAM_DISK_PATH/$volume_number"
    if [ -d "$TEMP_FOLDER_PATH" ] ; then
        removeTempFolderInRamDisk
    fi
    mkdir $TEMP_FOLDER_PATH

    logThis "1" "Created temporary folder $TEMP_FOLDER_PATH"
}

# The function copyFilesToRamDisk receives 1 argument.
# $1 is the volume number.
# This function should copy 2 files to RamDisk: Pi data and pagination.
function copyFilesToRamDisk {
    local volume_number="$1"
    TEMP_RAW_PI_FILE_PATH="$TEMP_FOLDER_PATH/pi.txt"
    TEMP_PAGENUMBERS_FILE_PATH="$TEMP_FOLDER_PATH/pagenumbers.txt"

    fold -w 54 "$RAW_PI_FILES_FOLDER_PATH/pi-$volume_number.txt" > "$TEMP_RAW_PI_FILE_PATH"
    logThis "1" "Copied pi-$volume_number.txt to $TEMP_RAW_PI_FILE_PATH"
    cp "$PAGENUMBER_FILES_FOLDER_PATH/$volume_number.txt" "$TEMP_PAGENUMBERS_FILE_PATH"
    logThis "1" "Copied pagenumbers-$volume_number.txt to $TEMP_PAGENUMBERS_FILE_PATH"
}

# The function renderPdfWithOsascript receives 2 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# This function should open the template.indt file previously copied to the ramdisk.
function renderPdfWithOsascript {
    local ram_disk_name="$1"
    local volume_number="$2"
    local volume_number_with_thousands_separator=$(printf "%'d" "$volume_number")

    HFS_PATH_TO_INDESIGN_TEMPLATE="$ram_disk_name:template.indt"
    HFS_PATH_TO_TEMP_RAW_PI_FILE="$ram_disk_name:$volume_number:pi.txt"
    HFS_PATH_TO_TEMP_PAGENUMBERS_FILE="$ram_disk_name:$volume_number:pagenumbers.txt"
    HFS_PATH_TO_TEMP_NEW_PDF="$ram_disk_name:$volume_number:cleanup-relabel-addnewmetadata-compress-$volume_number.pdf"

    logThis "1" "Opening template as hidden window in Indesign via osascript -e" 1

    osascript -e 'tell application "Adobe InDesign CC 2019"
            set myDocument to open alias "'$HFS_PATH_TO_INDESIGN_TEMPLATE'" without showing window

	        set myTitleTextFrame to text frame "title_frame" of page 1 of myDocument
	        set myStoryTextFrame to text frame "story_frame" of page 4 of myDocument
	        set myNumberFrame to text frame "sidnummer" of page 4 of myDocument
	        set myColophonTextFrame to text frame "colophon_frame" of page 391 of myDocument

            tell word 7 of parent story of myTitleTextFrame
				set contents to "'$volume_number_with_thousands_separator'"
			end tell

            tell paragraph 1 of myStoryTextFrame to delete
			if "'$volume_number'" = 1 then
				set contents of myStoryTextFrame to "3.
"
			end if
			tell insertion point -1 of myStoryTextFrame
				place file ("'$HFS_PATH_TO_TEMP_RAW_PI_FILE'") without showing options
			end tell
			set applied paragraph style of parent story of myStoryTextFrame to "Pi-siffror"

            tell parent story of myNumberFrame to delete
			tell insertion point -1 of myNumberFrame
				place file ("'$HFS_PATH_TO_TEMP_PAGENUMBERS_FILE'") without showing options
			end tell
			set applied paragraph style of parent story of myNumberFrame to "Sidnummer"

            tell word 7 of parent story of myColophonTextFrame
				set contents to "'$volume_number_with_thousands_separator'"
			end tell

            set outputPdfPath to "'$HFS_PATH_TO_TEMP_NEW_PDF'" as string
            tell myDocument to export format PDF type to outputPdfPath without showing options

            close myDocument saving no
        end tell'
        logThis "0" "Osascript part is done" 1
}

# The function cleanUpPdfMetadata receives 2 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# This function removes metadata from the newly rendered pdf.
function cleanUpPdfMetadata {
    local ram_disk_name="$1"
    local volume_number="$2"
    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/cleanup-relabel-addnewmetadata-compress-$volume_number.pdf"
    local pdf_path_after="/Volumes/$ram_disk_name/$volume_number/relabel-addnewmetadata-compress-$volume_number.pdf"

    logThis "1" "Removing all exif metadata from $pdf_path_before"
    exiftool -m -all:all= "$pdf_path_before"

    logThis "1" "Renaming $pdf_path_before to $pdf_path_after"
    mv $pdf_path_before $pdf_path_after
}

# The function relabelPdf receives 2 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# This function relabels the pages if we're on a volume number greater than 2592 which is what Indesign can do on its own.
function relabelPdf {
    local ram_disk_name="$1"
    local volume_number="$2"
    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/relabel-addnewmetadata-compress-$volume_number.pdf"
    local pdf_path_after="/Volumes/$ram_disk_name/$volume_number/addnewmetadata-compress-$volume_number.pdf"

	if (( "$volume_number" > 2592 )); then
        logThis "1" "Relabeling $pdf_path_before" 1
        logThis "1" "Calculating what page number to start with"
        STARTWITHPAGENUMBER=$((volume_number*386-385))

        logThis "1" "Unpacking to qdf format"
        qpdf --qdf "$pdf_path_before" "$pdf_path_before.temp1"

        logThis "1" "Sed relabeling"
        gsed "s/\/St 1111/\/St $STARTWITHPAGENUMBER/" < "$pdf_path_before.temp1" > "$pdf_path_before.temp2"

        logThis "1" "Converting back to pdf format and renaming to $pdf_path_after"
        fix-qdf < "$pdf_path_before.temp2" > "$pdf_path_after"
    else
        logThis "1" "Relabeling not needed. Renaming $pdf_path_before to $pdf_path_after"
        mv "$pdf_path_before" "$pdf_path_after"
    fi

    # logThis "1" "Setting xattr isRelabeled to 'yes' on $pdf_path_after"
    # xattr -w isRelabeled "yes" "$pdf_path_after"
}

# The function addNewPdfMetadata receives 2 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# This function adds metadata to the newly rendered pdf.
function addNewPdfMetadata {
    local ram_disk_name="$1"
    local volume_number="$2"
    local volume_number_with_thousands_separator=$(printf "%'d" "$volume_number")
    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/addnewmetadata-compress-$volume_number.pdf"
    local pdf_path_after="/Volumes/$ram_disk_name/$volume_number/compress-$volume_number.pdf"

    logThis "1" "Adding new metadata…" 1
    exiftool -m -Title="One Trillion Digits of Pi – Volume $volume_number_with_thousands_separator of 1,000,000" -Author="Lennart Grebelius" -Copyright="Copyleft" -Language="en_US" -Subject="In this work, π has been calculated to one trillion decimal places, which must be regarded as a precise approximation. I calculated the first 10 million digits on a laptop in my studio. Shigeru Kondo and Alexander Yee generously provided me with the remaining 999,990 million." "$pdf_path_before"

    logThis "1" "Renaming $pdf_path_before to $pdf_path_after"
    mv $pdf_path_before $pdf_path_after
}

# The function linearizeAndCompressPdf receives 2 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# This function linearizes (removes unneeded data) the newly rendered pdf.
function linearizeAndCompressPdf {
    local ram_disk_name="$1"
    local volume_number="$2"
    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/compress-$volume_number.pdf"
    local pdf_path_after="/Volumes/$ram_disk_name/$volume_number/$volume_number.pdf"

    logThis "1" "Linearizing…" 1
    qpdf --linearize --stream-data=compress "$pdf_path_before" "$pdf_path_after"
}

# The function savePdfToRenderFolder receives 3 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# $3 is the folder in which to put the finished pdf.
# This function linearizes (removes unneeded data) and compresses the newly rendered pdf.
function savePdfToRenderFolder {
    logThis "1" "Start of function" 1
    local ram_disk_name="$1"
    local volume_number="$2"

    local render_folder_path="$3"
    # local render_folder_path="$HOME/Desktop/temp" # Use for dev-mode

    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/$volume_number.pdf"
    local pdf_path_after="$render_folder_path/One-Trillion-Digits-of-Pi-$volume_number.pdf"

    # Warn if being in dev mode
    if [ "$render_folder_path" == "$HOME/Desktop/temp" ] ; then
        echo ""
        echo "  😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱"
        echo "  FWY, the render_folder_path is set to $render_folder_path."
        echo "  This is useful for debugging but will NOT place the finished"
        echo "  pdfs in their correct locations. Do not use for production."
        echo "  😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱"
        echo ""
    fi

    logThis "1" "Setting xattr isRelabeled to 'yes' on $pdf_path_before"
    xattr -w isRelabeled "yes" "$pdf_path_before"

    logThis "1" "Copying $pdf_path_before to $pdf_path_after"
    cp "$pdf_path_before" "$pdf_path_after"
}

# The function copyPdfToWebServer receives 2 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# This function linearizes (removes unneeded data) and compresses the newly rendered pdf.
function copyPdfToWebServer {
    logThis "1" "Start of function" 1
    local ram_disk_name="$1"
    local volume_number="$2"

    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/$volume_number.pdf"
    local pdf_path_after="$folder_path_on_web_server/One-Trillion-Digits-of-Pi-$volume_number.pdf"


}

# The function removeTempFolderInRamDisk receives 0 arguments.
# This function should remove $TEMP_FOLDER_PATH and all of its content on the RamDisk.
function removeTempFolderInRamDisk {
    rm -rf "$TEMP_FOLDER_PATH"
    logThis "1" "Removed temporary folder $TEMP_FOLDER_PATH with all of its content."
}
