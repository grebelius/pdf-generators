#!/usr/bin/env bash

# ----------------------------------------------------------------------------------
# Functions that relabel the pdfs with a little help from exiftool and qpdf
# ----------------------------------------------------------------------------------

# The function relabelFunction receives 2 arguments.
# $1 is the desired folder path for the pdf.
# $2 is the volume number.
function relabelFunction {
    local render_folder_path="$1"
    local volume_number="$2"

    # First, check if is marked as not in need of relabeling. If so, remove from index
    local path_original_pdf="$render_folder_path/One-Trillion-Digits-of-Pi-$volume_number.pdf"
    local xattr_args=(-p isRelabeled "$path_original_pdf")
    xattr_result=$(xattr "${xattr_args[@]}")
    if [ "$xattr_result" = "notNeeded" ] || [ "$xattr_result" = "yes" ]; then
        logThis "1" "$volume_number already relabeled" 1
        logThis "1" "Removing $volume_number from index" 1
        sed -i '' "/$volume_number/d" "$render_folder_path/_files_left_to_relabel.index"
        return 1
    fi

    createTempFolderInRamDisk $volume_number
    copyFileToRamDisk $render_folder_path $volume_number
    cleanUpPdfMetadata $RAM_DISK_VOLUME_NAME $volume_number
    relabelPdf $RAM_DISK_VOLUME_NAME $volume_number
    addNewPdfMetadata $RAM_DISK_VOLUME_NAME $volume_number
    linearizeAndCompressPdf $RAM_DISK_VOLUME_NAME $volume_number
    savePdfToRenderFolder $RAM_DISK_VOLUME_NAME $volume_number $render_folder_path
    removeTempFolderInRamDisk $volume_number
}

# The function createTempFolderInRamDisk receives 1 argument.
# $1 is the volume number.
# This function should create a temp folder on the RamDisk.
function createTempFolderInRamDisk {
    local volume_number="$1"

    TEMP_FOLDER_PATH="$RAM_DISK_PATH/$volume_number"
    if [ -d "$TEMP_FOLDER_PATH" ] ; then
        removeTempFolderInRamDisk
    fi
    mkdir $TEMP_FOLDER_PATH

    logThis "1" "Created temporary folder $TEMP_FOLDER_PATH"
}

# The function copyFileToRamDisk receives 2 arguments.
# $1 is the render folder path.
# $2 is the volume number.
# This function should copy 1 file to RamDisk: The pdf that needs relabeling.
function copyFileToRamDisk {
    local render_folder_path="$1"
    local volume_number="$2"
    path_original_pdf="$render_folder_path/One-Trillion-Digits-of-Pi-$volume_number.pdf"
    path_original_pdf_on_ramdisk="$TEMP_FOLDER_PATH/cleanup-relabel-addnewmetadata-compress-$volume_number.pdf"

    cp "$path_original_pdf" "$path_original_pdf_on_ramdisk"
    logThis "1" "Copied pdf $volume_number to ramdisk" 1
}

# The function cleanUpPdfMetadata receives 2 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# This function removes metadata from the newly rendered pdf.
function cleanUpPdfMetadata {
    local ram_disk_name="$1"
    local volume_number="$2"
    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/cleanup-relabel-addnewmetadata-compress-$volume_number.pdf"
    local pdf_path_after="/Volumes/$ram_disk_name/$volume_number/relabel-addnewmetadata-compress-$volume_number.pdf"

    logThis "1" "Removing all exif metadata from $pdf_path_before" 1
    exiftool -m -all:all= "$pdf_path_before"

    logThis "1" "Renaming $pdf_path_before to $pdf_path_after" 1
    mv $pdf_path_before $pdf_path_after
}

# The function relabelPdf receives 2 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# This function relabels the pages if we're on a volume number greater than 2592 which is what Indesign can do on its own.
function relabelPdf {
    local ram_disk_name="$1"
    local volume_number="$2"
    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/relabel-addnewmetadata-compress-$volume_number.pdf"
    local pdf_path_after="/Volumes/$ram_disk_name/$volume_number/addnewmetadata-compress-$volume_number.pdf"

	if (( "$volume_number" > 2592 )); then
        logThis "1" "Relabeling $pdf_path_before" 1
        logThis "1" "Calculating what page number to start with"
        STARTWITHPAGENUMBER=$((volume_number*386-385))

        logThis "1" "Unpacking to qdf format"
        qpdf --qdf "$pdf_path_before" "$pdf_path_before.temp1"

        logThis "1" "Sed relabeling"
        gsed "s/\/St 1111/\/St $STARTWITHPAGENUMBER/" < "$pdf_path_before.temp1" > "$pdf_path_before.temp2"

        logThis "1" "Converting back to pdf format and renaming to $pdf_path_after"
        fix-qdf < "$pdf_path_before.temp2" > "$pdf_path_after"
    else
        logThis "1" "Relabeling not needed. Renaming $pdf_path_before to $pdf_path_after"
        mv "$pdf_path_before" "$pdf_path_after"
    fi

    # logThis "1" "Setting xattr isRelabeled to 'yes' on $pdf_path_after"
    # xattr -w isRelabeled "yes" "$pdf_path_after"
}

# The function addNewPdfMetadata receives 2 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# This function adds metadata to the newly rendered pdf.
function addNewPdfMetadata {
    local ram_disk_name="$1"
    local volume_number="$2"
    local volume_number_with_thousands_separator=$(printf "%'d" "$volume_number")
    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/addnewmetadata-compress-$volume_number.pdf"
    local pdf_path_after="/Volumes/$ram_disk_name/$volume_number/compress-$volume_number.pdf"

    logThis "1" "Adding new metadata…" 1
    exiftool -m -Title="One Trillion Digits of Pi – Volume $volume_number_with_thousands_separator of 1,000,000" -Author="Lennart Grebelius" -Copyright="Copyleft" -Language="en_US" -Subject="In this work, π has been calculated to one trillion decimal places, which must be regarded as a precise approximation. I calculated the first 10 million digits on a laptop in my studio. Shigeru Kondo and Alexander Yee generously provided me with the remaining 999,990 million." "$pdf_path_before"

    logThis "1" "Renaming $pdf_path_before to $pdf_path_after"
    mv $pdf_path_before $pdf_path_after
}

# The function linearizeAndCompressPdf receives 2 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# This function linearizes (removes unneeded data) the newly rendered pdf.
function linearizeAndCompressPdf {
    local ram_disk_name="$1"
    local volume_number="$2"
    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/compress-$volume_number.pdf"
    local pdf_path_after="/Volumes/$ram_disk_name/$volume_number/$volume_number.pdf"

    logThis "1" "Linearizing…" 1
    qpdf --linearize --stream-data=compress "$pdf_path_before" "$pdf_path_after"
}

# The function savePdfToRenderFolder receives 3 arguments.
# $1 is $RAM_DISK_VOLUME_NAME, the name of the RamDisk
# $2 is the volume number
# $3 is the folder in which to put the finished pdf.
# This function linearizes (removes unneeded data) and compresses the newly rendered pdf.
function savePdfToRenderFolder {
    logThis "1" "Start of function" 1
    local ram_disk_name="$1"
    local volume_number="$2"

    local render_folder_path="$3"
    # local render_folder_path="$HOME/Desktop/temp" # Use for dev-mode

    local pdf_path_before="/Volumes/$ram_disk_name/$volume_number/$volume_number.pdf"
    local pdf_path_after="$render_folder_path/One-Trillion-Digits-of-Pi-$volume_number.pdf"

    # Warn if being in dev mode
    if [ "$render_folder_path" == "$HOME/Desktop/temp" ] ; then
        echo ""
        echo "  😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱"
        echo "  FWY, the render_folder_path is set to $render_folder_path."
        echo "  This is useful for debugging but will NOT place the finished"
        echo "  pdfs in their correct locations. Do not use for production."
        echo "  😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱"
        echo ""
    fi

    logThis "1" "Setting xattr isRelabeled to 'yes' on $pdf_path_before"
    xattr -w isRelabeled "yes" "$pdf_path_before"

    logThis "1" "Copying $pdf_path_before to $pdf_path_after"
    cp "$pdf_path_before" "$pdf_path_after"
}

# The function removeTempFolderInRamDisk receives 0 arguments.
# This function should remove $TEMP_FOLDER_PATH and all of its content on the RamDisk.
function removeTempFolderInRamDisk {
    rm -rf "$TEMP_FOLDER_PATH"
    logThis "1" "Removed temporary folder $TEMP_FOLDER_PATH with all of its content."
}
