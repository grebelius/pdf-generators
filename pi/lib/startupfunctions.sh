#!/bin/sh

# -----------------------------------------------------------------------------------------
# Functions that make sure that dependencies and fonts are installed. And launces Indesign.
# -----------------------------------------------------------------------------------------

function launchIndesign {
    logThis "1" "Launching Indesign in the background…" 1
    open -F -g -a Adobe\ InDesign\ CC\ 2019
}

function checkIfDependenciesAreInstalled {
    logThis "1" "Checking dependencies…"

    dependencies_are_missing=false

    if command -v parallel >/dev/null 2>&1 ; then
        echo " ✔︎ GNU Parallel"
    else
        logThis "2" "GNU Parallel (please install with 'brew install parallel')" 1
        dependencies_are_missing=true
    fi

    if command -v exiftool >/dev/null 2>&1 ; then
        echo " ✔︎ Exiftool"
    else
        logThis "2" "Exiftool (please install with 'brew install exiftool')" 1
        dependencies_are_missing=true
    fi

    if command -v gsed >/dev/null 2>&1 ; then
        echo " ✔︎ GNU sed"
    else
        logThis "2" "GNU sed (please install with 'brew install gnu-sed')" 1
        dependencies_are_missing=true
    fi

    if command -v qpdf >/dev/null 2>&1 ; then
        echo " ✔︎ Qpdf"
    else
        logThis "2" "Qpdf (please install with 'brew install qpdf')" 1
        dependencies_are_missing=true
    fi

    if "$dependencies_are_missing" ; then
        logThis "2" "Exiting script…" 1
        exit 1
    fi
}

function checkIfIndicesExists {
    logThis "1" "Checking indices…"

    if [ -f "$RENDER_FOLDER_PATH/_render_folders.index" ] ; then
        while IFS= read -r sub_folder || [ -n "$sub_folder" ]; do
            if [ ! -f "$sub_folder/_missing_files.index" ] ; then
                logThis "1" "Couldn't find $sub_folder/_missing_files.index"
            fi
            if [ ! -f "$sub_folder/_files_left_to_relabel.index" ] ; then
                logThis "1" "Couldn't find $sub_folder/_files_left_to_relabel.index"
            fi
        done < "$RENDER_FOLDER_PATH/_render_folders.index"
    else
        logThis "2" "Couldn't find $RENDER_FOLDER_PATH/_render_folders.index" 1
        logThis "2" "Exiting script…" 1
        exit 1
    fi
}

function checkIfFontsAreInstalled {
    logThis "1" "Checking installed fonts…" 1
    if system_profiler SPFontsDataType | grep -i bembo >/dev/null; then
        echo " ✔︎ Bembo"
    else
        logThis "2" "Bembo (please install from bembo.zip in this repo)" 1
        logThis "2" "Exiting script…" 1
        exit 1
    fi
}