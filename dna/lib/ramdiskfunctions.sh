#!/bin/sh

# ---------------------------------------------------------------------------
# Functions that create the ram disk and copies the Indesign template onto it
# ---------------------------------------------------------------------------

function createRamDiskAndCopyFiles {
    logThis "1" "Start of function"

    local volume_name=DNA-RamDisk-$(date '+%Y-%m-%d_%H.%M.%S')
    local ram_disk_volume_name="$volume_name"
    local size_in_mb=200
    local device=`hdiutil attach -nobrowse -nomount ram://$(($size_in_mb*2048))`

    diskutil erasevolume HFS+ $ram_disk_volume_name $device

    logThis "1" "Copying template to $ram_disk_volume_name"

    export ram_disk_path="/Volumes/$ram_disk_volume_name"
    export template_file_path="$ram_disk_path/template.indt"

    local template_on_nas_path="$konstnas/books/template"
    cp "$template_on_nas_path/template.indt" "$template_file_path"
    if [ ! -f "$template_file_path" ] ; then
        logThis "2" "Copy of template.indt failed" 1
        logThis "2" "Exiting script…" 1
        exit 1
    fi
    logThis "1" "End of function"
}

function ramDiskForDevMode {
    logThis "1" "Start of function"
    logThis "1" "Assuming RamDisk is already mounted and template file copied."

    local volume_name="$1"
    local ram_disk_volume_name="$volume_name"

    export ram_disk_path="/Volumes/$ram_disk_volume_name"
    export template_file_path="$ram_disk_path/template.indt"

    local template_on_nas_path="$konstnas/books/template"
    cp "$template_on_nas_path/template.indt" "$template_file_path"
    if [ ! -f "$template_file_path" ] ; then
        logThis "2" "Copy of template.indt failed" 1
        logThis "2" "Exiting script…" 1
        exit 1
    fi

    logThis "1" "ram_disk_volume_name is set to $ram_disk_volume_name"
    logThis "1" "ram_disk_path is set to $ram_disk_path"
    logThis "1" "End of function"
}